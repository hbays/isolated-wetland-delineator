# Isolated Wetland Delineator

Finds isolated wetlands in Indiana counties

Wetlands are a crucial factor in our environmental landscape, providing significant benefits including assimilation of pollutants, flood water storage, groundwater recharge,  and fish and wildlife habitat. Wetlands are constantly threatened with degradation by conversion to agriculture or urban development. A hot topic of political interest is the protection of isolated wetlands. Isolated wetlands are geographically isolated and have seemingly no connection to viable hydrological influencers; however, provide the same benefits as described above. With my project, I would like to create an isolated wetland suitability model to locate areas where isolated wetlands may exist. This may help prove to political powers that isolated wetlands do exist in abundance and they should be protected.

# Inputs

The user would need to provide the following inputs in order for the tool to work:
    1. DEM of area of interest
    2. Project Area within that DEM
    3. Soil survey feature layer within project area
    4. NHD layer
    5. National Wetland Inventory (NWI) layer

# How the Tool Works
This tool is an assessment of the digital elevation model (DEM) to identify and interpolate polygon areas of low elevation or sink spots. Following the generation of sinks, areas within hydric soils and known wetland areas are identified.

Areas with obvious hydrological connections are removed.

The outputs of this project would be areas that have the potential to be isolated wetlands. 

# Roadblocks
Roadblocks included downloading a DEM in Indiana, there's not a lot available. The NWI layer would not download from the source for two days. 
